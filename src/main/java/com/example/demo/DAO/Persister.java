package com.example.demo.DAO;

import javax.transaction.Transactional;

import org.springframework.context.ApplicationContext;

import com.example.demo.Task1Application;
import com.example.demo.bean.Employee;
import com.example.demo.repository.EmpRepo;

public class Persister {

	ApplicationContext context = Task1Application.ap;

	EmpRepo factory = context.getBean(EmpRepo.class);

	@Transactional
	public Employee add(Employee emp) {
		Employee res = factory.save(emp);

		return res;

	}

}
