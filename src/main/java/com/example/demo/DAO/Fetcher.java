package com.example.demo.DAO;

import java.util.List;

import org.springframework.context.ApplicationContext;

import com.example.demo.Task1Application;
import com.example.demo.bean.Employee;
import com.example.demo.repository.EmpRepo;

public class Fetcher {

	ApplicationContext context = Task1Application.ap;

	EmpRepo factory = context.getBean(EmpRepo.class);

	public List<Employee> get() {
		List<Employee> res = factory.findAllEmployee();

		return res;

	}

}
