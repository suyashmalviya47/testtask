package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.example.demo.bean")
@EnableJpaRepositories("com.example.demo.repository")
public class Task1Application {

	public static ConfigurableApplicationContext ap;

	public static void main(String[] args) {
		ap = SpringApplication.run(Task1Application.class, args);
	}

}
