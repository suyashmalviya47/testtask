package com.example.demo.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DAO.Fetcher;
import com.example.demo.DAO.Persister;
import com.example.demo.bean.Employee;

@RestController
public class MainController {

	@PostMapping("/add")
	public ResponseEntity<Object> add(@RequestBody Employee emp) {

		Persister p = new Persister();
		Employee res = p.add(emp);

		HttpHeaders headers = new HttpHeaders();
		headers.add("employee", res.toString());

		return new ResponseEntity<Object>("Custom header set", headers, HttpStatus.OK);
	}

	@GetMapping("/get")
	public ResponseEntity<Object> add() {

		Fetcher f = new Fetcher();
		List<Employee> res = f.get();
		HttpHeaders headers = new HttpHeaders();
		headers.add("employee", res.toString());

		return new ResponseEntity<Object>("Custom header set", headers, HttpStatus.OK);
	}

}
