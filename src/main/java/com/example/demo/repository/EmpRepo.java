package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.bean.Employee;

@Repository
public interface EmpRepo extends JpaRepository<Employee, Integer> {

	@Query(value = "SELECT * FROM employee Where state in ('MAharastra','TamilNadu','Andhrapradesh') AND age<30 ORDER BY name", nativeQuery = true)
	public List<Employee> findAllEmployee();
}
